import axios from 'axios';
import { AsyncStorage } from 'react-native';

// export const SIGNUP = 'SIGNUP';
// export const LOGIN = 'LOGIN';
export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';

let timer;
export const authenticate = (userId, token, expiryTime) => {

  return dispatch => {
    dispatch(setLogoutTimer(expiryTime));
    dispatch({ type: AUTHENTICATE, userId: userId, token: token });
  }
}

export const signup = (email, password) => {
  return async dispatch => {
    try {
      const response = await axios.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyB0ZDDvTOWnBPggig7kbSl2wX4-vmD20kQ',
        {
          email: email,
          password: password,
          returnSecureToken: true
        }
      );
      //  console.log('response', response)
      if (response.data) {
        dispatch(authenticate(response.data.localId, response.data.idToken, parseInt(response.data.expiresIn) * 1000));
        const expirationDate = new Date(new Date().getTime() + parseInt(response.data.expiresIn) * 1000);
        saveDataToStorage(response.data.idToken, response.data.localId, expirationDate)
      }
    } catch (error) {
      const errorId = error.response.data.error.message;
      let message = "Something went wrong"
      if (errorId === 'EMAIL_EXISTS') {
        message = 'this email already exists';
      }
      throw new Error(message);
    }
  }
};

export const login = (email, password) => {

  return async dispatch => {
    try {
      const response = await axios.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB0ZDDvTOWnBPggig7kbSl2wX4-vmD20kQ',
        {
          email: email,
          password: password,
          returnSecureToken: true
        }
      );
      if (response.data) {
        dispatch(authenticate(response.data.localId, response.data.idToken, parseInt(response.data.expiresIn) * 1000));
        const expirationDate = new Date(new Date().getTime() + parseInt(response.data.expiresIn) * 1000);
        saveDataToStorage(response.data.idToken, response.data.localId, expirationDate)
      }
    } catch (error) {
      const errorId = error.response.data.error.message;
      let message = "Something went wrong"
      if (errorId === 'EMAIL_NOT_FOUND') {
        message = 'this email could not be found';
      } else if (errorId === 'INVALID_PASSWORD') {
        message = 'the password is not valid!'
      }
      throw new Error(message);
    }
  };
};

export const logout = () => {
  //console.log('in logout')
  clearLogoutTimer();
  AsyncStorage.removeItem('userData');
  return { type: LOGOUT }
}

const clearLogoutTimer = () => {
  if (timer) {
    clearTimeout(timer);
  }
}
const setLogoutTimer = expirationDate => {
  console.log('setlogouttimer : ', expirationDate)
  return dispatch => {
    timer = setTimeout(() => {
      dispatch(logout());
    }, expirationDate)
  }

}
const saveDataToStorage = async (token, userId, expirationDate) => {

  let userData = await AsyncStorage.setItem('userData', JSON.stringify({
    token: token,
    userId: userId,
    expirationDate: expirationDate.toISOString()
  }))
  //console.log('SaveData userData: ', userData)
}