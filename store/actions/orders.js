import axios from "axios"
import Order from "../../models/order";
export const ADD_ORDER = 'ADD_ORDER';
export const SET_ORDERS = 'SET_ORDERS';


export const fetchOrders = () => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    const userId = getState().auth.userId;
    try {
      const response = await axios.get(
        `https://shpping-app.firebaseio.com/orders/${userId}.json`
      );


      const loadedOrders = [];
      const resData = response.data
      for (const key in resData) {
        loadedOrders.push(
          new Order(
            key,
            resData[key].cartItems,
            resData[key].totalAmount,
            new Date(resData[key].date)
          )
        );
      }
      dispatch({ type: SET_ORDERS, orders: loadedOrders })
    } catch (err) {
      console.log('order.fetch', err)
      throw new Error('Something goes wrong')

    }
  }
}

export const addOrder = (cartItems, totalAmount) => {
  const date = new Date();
  return async (dispatch, getState) => {
    try {
      const token = getState().auth.token;
      const userId = getState().auth.userId;
      const response = await axios.post(
        `https://shpping-app.firebaseio.com/orders/${userId}.json?auth=${token}`,
        {
          cartItems,
          totalAmount,
          date: date.toISOString()
        })

      const resData = response.data

      dispatch({
        type: ADD_ORDER,
        orderData: {
          id: resData.name,
          items: cartItems,
          amount: totalAmount,
          date: date
        }
      });
    } catch (err) {

      throw new Error('something went wrong')
    }
  }

};
