import Product from "../../models/product";
import axios from "axios";

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const SET_PRODUCTS = 'SET-PRODUCTS';

export const fetchProducts = () => {
  try {
    return async (dispatch, getState) => {
      const userId = getState().auth.userId;
      const response = await axios.get(
        'https://shpping-app.firebaseio.com/producs.json'
      );
      const resData = response.data;
      //console.log('resData', resData)
      if (response.data) {
        const loadedProducts = [];

        for (const key in resData) {
          loadedProducts.push(new Product(
            key,
            resData[key].ownerId,
            resData[key].title,
            resData[key].imageUrl,
            resData[key].description,
            resData[key].price
          ))
        }
        dispatch({
          type: SET_PRODUCTS,
          products: loadedProducts,
          userProducts: loadedProducts.filter(prod => prod.ownerId === userId)
        })
      }

    }
  } catch (error) {
    const errorId = error.response.data.error.message;
    throw new Error(errorId);
  }

}

export const deleteProduct = productId => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    const response = await axios.delete(`https://shpping-app.firebaseio.com/producs/${productId}.json?auth=${token}`,

    )
    //console.log('respons.data1', response)
    dispatch({ type: DELETE_PRODUCT, pid: productId })
  }
};

export const createProduct = (title, description, imageUrl, price) => {
  return async (dispatch, getState) => {
    try {
      const token = getState().auth.token;
      const userId = getState().auth.userId;
      const response = await axios.post(`https://shpping-app.firebaseio.com/producs.json?auth=${token}`,
        {
          title,
          description,
          imageUrl,
          price,
          ownerId: userId
        })
      if (response.data) {
        dispatch({
          type: CREATE_PRODUCT,
          productData: {
            id: response.data,
            title,
            description,
            imageUrl,
            price,
            ownerId: userId
          }
        });
      }
    } catch (error) {
      throw new Error(error.response.data.error.message)
    }
  }
};

export const updateProduct = (id, title, description, imageUrl) => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    try {
      await axios.patch(`https://shpping-app.firebaseio.com/producs/${id}.json?auth=${token}`,
        {
          title,
          description,
          imageUrl,
        }

      );
      dispatch({
        type: UPDATE_PRODUCT,
        pid: id,
        productData: {
          title,
          description,
          imageUrl
        }
      });
    } catch (error) {
      throw new Error('token is  not valid!');
    }



  };
};
